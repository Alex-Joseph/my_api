class ApplicationController < ActionController::API
  require 'jwt'

  before_action :authenticate_user

  private

  def authenticate_user
    hmac_secret = 'my$ecretK3y'
    decoded_token = JWT.decode params[:token], hmac_secret, true, { algorithm: 'HS256' }
    p decoded_token
    if decoded_token[0]['secret'] != 'secret'
      render json: "unauthorized", status: 403
    end
  end
end
