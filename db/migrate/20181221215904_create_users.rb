class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :first_name, null: false
      t.string :last_name
      t.string :username, null: false
      t.string :password_digest
      t.string :recovery_token
      t.string :expiring_token

      t.timestamps
    end
  end
end
